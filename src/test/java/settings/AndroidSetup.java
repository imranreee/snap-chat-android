package settings;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by imran on 06/11/2018.
 */
public class AndroidSetup {
    public static AndroidDriver driver;

    public static void prepareAndroidForSnapChat() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("deviceName", "Android");
        capabilities.setCapability("platformVersion", "8.1");
        capabilities.setCapability("autoGrantPermissions", "true");

        capabilities.setCapability("unicodeKeyboard", "true");
        capabilities.setCapability("resetKeyboard", "true");
        //capabilities.setCapability("noReset", "true");

        capabilities.setCapability("appPackage", "com.snapchat.android");
        capabilities.setCapability("appActivity", "com.snapchat.android.LandingPageActivity");

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
    }


    public static void prepareAndroidForVpn() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("deviceName", "Android");
        capabilities.setCapability("platformVersion", "8.1");
        capabilities.setCapability("autoGrantPermissions", "true");

        capabilities.setCapability("unicodeKeyboard", "true");
        capabilities.setCapability("resetKeyboard", "true");
        capabilities.setCapability("noReset", "true");

        capabilities.setCapability("appPackage", "com.protonvpn.android");
        capabilities.setCapability("appActivity", "com.protonvpn.android.ui.onboarding.SplashActivity");

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
    }
}
