package settings;


import etc.SnapChat;
import etc.UpAndRunVpn;
import org.testng.annotations.*;

/**
 * Created by imran on 06/12/2018.
 */
public class RunCases extends AndroidSetup {

    boolean value = true;

    @BeforeMethod
    public void setUp() throws Exception {
        if (value == true){
            prepareAndroidForSnapChat();
        }else {
            prepareAndroidForVpn();
        }
    }

    @Test(priority = 1)
    public void snapChatTest() throws Exception {
        new SnapChat(driver).snapChat();
        value = false;
    }

    @Test(priority = 2)
    public void upAndRunVpnTest() throws Exception {
        new UpAndRunVpn(driver).upAndRunVpn();
        value = true;
    }

    /*@Test(priority = 3)
    public void mapNavigateTest() throws Exception {
        new MapNavigate(driver).mapNavigate();
    }

    @Test(priority = 4)
    public void filterTest() throws Exception {
        new Filter(driver).filter();
    }*/



    @AfterMethod
    public void tearDown() {
        driver.quit();
    }
}
