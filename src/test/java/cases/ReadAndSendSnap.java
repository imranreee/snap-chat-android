package cases;

/*
 *  ****************************************************************************
 *  * Created by : Al Imran on 6/25/2018 at 1:41 PM.
 *  * Email : imranreee@gmail.com
 *  *
 *  * Last edited by : Al Imran on 6/25/2018.
 *  *
 *  * Last Reviewed by : <Reviewer Name> on <mm/dd/yy>
 *
 *     This class for reading and sending snap
 *  ****************************************************************************
 */

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import settings.ExcelRead;
import settings.ExcelWrite;
import settings.HelperClass;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

public class ReadAndSendSnap {

    protected AndroidDriver driver;
    int rowNo = 1;
    protected String excelPath = System.getProperty("user.dir") +"\\excelFiles\\\\data.xlsx";

    String expressVpnAppPackage = "com.expressvpn.vpn";
    String expressVpnAppActivity = "com.expressvpn.vpn.MainActivity";

    String snapChatAppPackage = "com.snapchat.android";
    String snapChatAppActivity = "com.snapchat.android.LandingPageActivity";

    By logInBtn = By.id("login_and_signup_page_fragment_login_button");
    By usernameOrEmailField = By.id("username_or_email_field");
    By passwordField = By.id("password_field");
    By signInBtn = By.id("registration_nav_container");

    By friendsBtn = By.id("hova_nav_feed_layout");
    By mediaGalleryButton = By.id("media_gallery_button");
    By selectImage = By.id("gallery_grid_item_selection_frame");

    By chooseLocationBtn = By.id("com.expressvpn.vpn:id/location_picker");
    By connectedStatusOn = By.xpath("//*[@text='VPN is ON']");
    By errorMessageLogin = By.id("login_email_or_username_or_password_error_message");

    By continueBtn = By.id("android:id/button1");
    By recentTab = By.xpath("//*[@text='RECENTS']");

    By newSnap = By.xpath("//*[@text='New Snap']");
    By newMessageBtn = By.id("neon_header_new_chat_button");
    By sendImageBtn = By.id("memories_drawer_send");

    By backToTheList = By.id("header_button");
    By crossBtn = By.id("chat_v10_cancel_mischief_btn");
    By searchResultFriend = By.id("primary_text");

    By friendSearchField = By.id("neon_header_feed_text");
    By noResultsTextView = By.id("no_results_text_view");

    int sizeBeforeScrolling;
    int sizeAfterScrolling;
    TreeSet<String> treeSet = new TreeSet<>();
    Boolean counter = false;


    @BeforeMethod
    public void setUp() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("deviceName", "Android");
        capabilities.setCapability("platformVersion", "8.1");
        capabilities.setCapability("autoGrantPermissions", "true");

        capabilities.setCapability("unicodeKeyboard", "true");
        capabilities.setCapability("resetKeyboard", "true");
        //capabilities.setCapability("noReset", "true");

        capabilities.setCapability("appPackage", "com.snapchat.android");
        capabilities.setCapability("appActivity", "com.snapchat.android.LandingPageActivity");

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
    }


    @Test(invocationCount = 2) //Put the number, How may times/For how may snap chat account do you want to run
    public void acceptAndSendFriendRequest() throws Exception{
        rowNo = rowNo + 1;

        waitForVisibilityOf(logInBtn);
        System.out.println("SnapChat up and running");
        driver.closeApp();

        //Opening ExpressVPN app
        Activity activity = new Activity(expressVpnAppPackage, expressVpnAppActivity);
        driver.startActivity(activity);

        waitForVisibilityOf(chooseLocationBtn);
        System.out.println("ExpressVPN app up and running");

        driver.findElement(chooseLocationBtn).click();
        waitForVisibilityOf(recentTab);
        driver.findElement(recentTab).click();
        Thread.sleep(1000);
        String locationName = ExcelRead.readData(excelPath, "0", rowNo, "4");
        By selectLocation = By.xpath(locationName);
        boolean chk = true;

        if (driver.findElements(selectLocation).size() > 0){
            driver.findElement(selectLocation).click();

            Thread.sleep(2000);
            if (driver.findElements(continueBtn).size() > 0){
                driver.findElement(continueBtn).click();
            }
            waitForVisibilityOf(connectedStatusOn);
        }else {
            while (chk == true){
                swipeVertical(driver,0.9,0.1,0.5,3000);
                Thread.sleep(1000);
                if (driver.findElements(selectLocation).size() > 0){
                    driver.findElement(selectLocation).click();
                    waitForVisibilityOf(connectedStatusOn);
                    chk = false;
                }
            }
        }
        System.out.println("VPN connected to: "+locationName);
        Thread.sleep(1000);
        driver.navigate().back();

        //Opening snap chat app again
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("deviceName", "Android");
        capabilities.setCapability("platformVersion", "8.1");
        capabilities.setCapability("autoGrantPermissions", "true");

        capabilities.setCapability("unicodeKeyboard", "true");
        capabilities.setCapability("resetKeyboard", "true");

        capabilities.setCapability("appPackage", snapChatAppPackage);
        capabilities.setCapability("appActivity", snapChatAppActivity);

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);


        waitForVisibilityOf(logInBtn);
        driver.findElement(logInBtn).click();
        driver.findElement(usernameOrEmailField).click();

        String snapChatID = ExcelRead.readData(excelPath, "0", rowNo, "2");
        driver.findElement(usernameOrEmailField).sendKeys(snapChatID);
        System.out.println("SnapChat id copied from excel: "+snapChatID);

        String snapChatPW = ExcelRead.readData(excelPath, "0", rowNo, "3");
        driver.findElement(passwordField).click();
        driver.findElement(passwordField).sendKeys(snapChatPW);
        System.out.println("SnapChat PW copied from excel");

        driver.findElement(signInBtn).click();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);


        if (driver.findElements(errorMessageLogin).size() > 0){
            System.out.println("User Id or password or something wrong: "+snapChatID);
        }else {
            waitForVisibilityOf(friendsBtn);
            System.out.println("Logged in successfully for: "+snapChatID);


            driver.findElement(friendsBtn).click();
            Thread.sleep(3000);

            boolean newSnapChk = true;

            while (newSnapChk == true){
                if (driver.findElements(newSnap).size() > 0){
                    driver.findElement(newSnap).click();
                    Thread.sleep(2000);
                    System.out.println("Incoming snap readed");
                    driver.navigate().back();

                    Thread.sleep(1000);
                    if (driver.findElements(newSnap).size() > 0){
                        newSnapChk = true;
                        System.out.println("True");
                    }else {
                        newSnapChk = false;
                        System.out.println("No more snap");
                    }
                }else {
                    newSnapChk = false;
                    System.out.println("No new snap");
                }
            }

            waitForVisibilityOf(newMessageBtn);
            driver.findElement(newMessageBtn).click();
            Thread.sleep(1000);

            List <WebElement> list1 = driver.findElements(By.id("name"));

            while (chk == true){
                sizeBeforeScrolling = treeSet.size();

                if (counter == true){
                    swipeVertical(driver, 0.7, 0.2, 0.5, 3000);
                    Thread.sleep(1000);
                }else {
                    swipeVertical(driver, 0.7, 0.5, 0.5, 3000);
                    counter = true;
                }

                for(int i = 0; i < list1.size(); i++){
                    String nameStr = list1.get(i).getText();
                    treeSet.add(nameStr);
                }

                sizeAfterScrolling = treeSet.size();
                if ((sizeBeforeScrolling - sizeAfterScrolling) == 0){
                    chk = false;
                }
            }

            int totalNumberOfFriend = treeSet.size();

            System.out.println("Total number of Friend = "+totalNumberOfFriend);
            Iterator<String> iterator = treeSet.iterator();

            driver.findElement(crossBtn).click();
            waitForVisibilityOf(friendSearchField);
            System.out.println("On the friends page");

            while (iterator.hasNext()){
                String item = iterator.next();

                driver.findElement(friendSearchField).click();
                Thread.sleep(1000);
                driver.findElement(friendSearchField).clear();
                driver.findElement(friendSearchField).sendKeys(item);
                System.out.println("Name = "+item);

                Thread.sleep(3000);
                if (driver.findElements(noResultsTextView).size() > 0){
                    System.out.println("No Result found");
                }else {
                    Thread.sleep(1000);
                    driver.findElement(searchResultFriend).click();
                    waitForVisibilityOf(mediaGalleryButton);
                    driver.findElement(mediaGalleryButton).click();

                    waitForVisibilityOf(selectImage);
                    driver.findElement(selectImage).click();

                    waitForVisibilityOf(sendImageBtn);
                    driver.findElement(sendImageBtn).click();
                    Thread.sleep(3000);
                    Thread.sleep(3000);

                    System.out.println("Snap sent.. to: "+item);

                    driver.findElement(backToTheList).click();
                    waitForVisibilityOf(friendSearchField);
                }

            }
        }
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

    protected void waitForVisibilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }


    public static void swipeVertical(AppiumDriver driver, double startPercentage, double finalPercentage, double anchorPercentage, int duration) throws Exception {
        Dimension size = driver.manage().window().getSize();
        int anchor = (int) (size.width * anchorPercentage);
        int startPoint = (int) (size.height * startPercentage);
        int endPoint = (int) (size.height * finalPercentage);
        new TouchAction(driver).press(anchor, startPoint).waitAction(Duration.ofMillis(duration)).moveTo(anchor, endPoint).release().perform();
    }

}
