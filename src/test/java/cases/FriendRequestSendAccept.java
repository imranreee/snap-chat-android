package cases;

/*
 *  ****************************************************************************
 *  * Created by : Al Imran on 6/25/2018 at 1:41 PM.
 *  * Email : imranreee@gmail.com
 *  *
 *  * Last edited by : Al Imran on 6/25/2018.
 *  *
 *  * Last Reviewed by : <Reviewer Name> on <mm/dd/yy>
 *
 *     This class accepting and sending friend request
 *  ****************************************************************************
 */

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import settings.ExcelRead;
import settings.ExcelWrite;
import settings.HelperClass;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class FriendRequestSendAccept {
    protected AndroidDriver driver;
    protected String excelPath = System.getProperty("user.dir") +"\\excelFiles\\\\data.xlsx";
    protected String excelPathReport = System.getProperty("user.dir") +"\\excelFiles\\report.xlsx";
    protected String excelPathBlockList = System.getProperty("user.dir") +"\\excelFiles\\blockList.xlsx";

    String expressVpnAppPackage = "com.expressvpn.vpn";
    String expressVpnAppActivity = "com.expressvpn.vpn.MainActivity";

    String snapChatAppPackage = "com.snapchat.android";
    String snapChatAppActivity = "com.snapchat.android.LandingPageActivity";

    By logInBtn = By.id("login_and_signup_page_fragment_login_button");
    By usernameOrEmailField = By.id("username_or_email_field");
    By passwordField = By.id("password_field");
    By signInBtn = By.id("registration_nav_container");

    By friendsBtn = By.id("hova_nav_feed_layout");
    By chooseLocationBtn = By.id("com.expressvpn.vpn:id/location_picker");
    By connectedStatusOn = By.xpath("//*[@text='VPN is ON']");

    By myProfile = By.id("bitmoji");
    By myProfile2 = By.id("arc_view");
    By addFriendsBtn = By.id("profile_v3_add_friends_button_header_label");
    By acceptBtn = By.id("friend_checkbox_image_container");

    By searchField = By.id("add_friends_v3_header_search_text_view");
    By noSearchResultMsg = By.id("no_results_text_view");
    By addBtn = By.id("friend_checkbox_button");
    By snapBtn = By.id("feed_reply_small_icon_text");
    By errorMessageLogin = By.id("login_email_or_username_or_password_error_message");

    By continueBtn = By.id("android:id/button1");
    By recentTab = By.xpath("//*[@text='RECENTS']");
    By newMessageBtn = By.id("neon_header_new_chat_button");
    By crossBtn = By.id("chat_v10_cancel_mischief_btn");
    By cancelBtn = By.id("no_button");
    By backToFriend = By.id("neon_header_back_button");

    By friendSearchField = By.id("neon_header_feed_text");
    By findUsername = By.id("description_text");
    By searchResult = By.id("arc_view");
    By settings = By.id("mini_profile_settings_text");
    By yesBtn = By.id("yes_button");
    By blockBtn = By.xpath("//*[@text='Block']");
    By noResultsTextView = By.id("no_results_text_view");
    By searchReault = By.id("primary_text");
    String snapChatAc;


    TreeSet<String> treeSet = new TreeSet<>();
    Boolean chk = true;
    int sizeBeforeScrolling;
    int sizeAfterScrolling;
    Boolean counter = false;

    int friendRowNo = 2;
    int rowNo = 1;
    int low = 1;


    int high = 159; //Change the height number, if increase the friend list
    int adjustNumberOfFriend = 15; //if numbers of friends below x (adjustable) add a random friend from list
    int maxNumberOfFriend = 2; //How many random friend request do you want to send in one run
    int blockFriendNumber = 3; //Put the number, how many friend do you want to block from block list

    //######### Please fill up below credentials #########

    String reportSendTo = "example@gmail.com"; //To which address do you want to send the report
    String emailId = "example@gmail.com"; // From which address do you want to send the report
    String emailPassword = "password"; //Password of above email id



    @BeforeMethod
    public void setUp() throws Exception {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("deviceName", "Android");
        capabilities.setCapability("platformVersion", "8.1");
        capabilities.setCapability("autoGrantPermissions", "true");

        capabilities.setCapability("unicodeKeyboard", "true");
        capabilities.setCapability("resetKeyboard", "true");
        //capabilities.setCapability("noReset", "true");

        capabilities.setCapability("appPackage", snapChatAppPackage);
        capabilities.setCapability("appActivity", snapChatAppActivity);

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
    }



    @Test(invocationCount = 1) //Put the number, For how may snap chat account do you want to run
    public void acceptAndSendFriendRequest() throws Exception{
        rowNo = rowNo + 1;

        waitForVisibilityOf(logInBtn);
        System.out.println("SnapChat up and running");
        driver.closeApp();

        //###### For clearing 'friendList' excel file
        for(int i = 2; i < 200; i++){
            ExcelWrite.writeData(excelPathReport, 0, i, 3, " ");
            ExcelWrite.writeData(excelPathReport, 0, i, 4, " ");
        }

        //Opening ExpressVPN app
        Activity activity = new Activity(expressVpnAppPackage, expressVpnAppActivity);
        driver.startActivity(activity);

        waitForVisibilityOf(chooseLocationBtn);
        System.out.println("ExpressVPN app up and running");

        driver.findElement(chooseLocationBtn).click();
        waitForVisibilityOf(recentTab);
        driver.findElement(recentTab).click();
        Thread.sleep(1000);
        String locationName = ExcelRead.readData(excelPath, "0", rowNo, "4");
        By selectLocation = By.xpath(locationName);
        boolean chk = true;

        if (driver.findElements(selectLocation).size() > 0){
            driver.findElement(selectLocation).click();

            Thread.sleep(2000);
            if (driver.findElements(continueBtn).size() > 0){
                driver.findElement(continueBtn).click();
            }
            waitForVisibilityOf(connectedStatusOn);
        }else {
            while (chk == true){
                swipeVertical(driver,0.9,0.1,0.5,3000);
                Thread.sleep(1000);
                if (driver.findElements(selectLocation).size() > 0){
                    driver.findElement(selectLocation).click();
                    waitForVisibilityOf(connectedStatusOn);
                    chk = false;
                }
            }
        }
        System.out.println("VPN connected to: "+locationName);
        Thread.sleep(1000);
        driver.navigate().back();

        //Opening snap chat app again
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("deviceName", "Android");
        capabilities.setCapability("platformVersion", "8.1");
        capabilities.setCapability("autoGrantPermissions", "true");

        capabilities.setCapability("unicodeKeyboard", "true");
        capabilities.setCapability("resetKeyboard", "true");

        capabilities.setCapability("appPackage", snapChatAppPackage);
        capabilities.setCapability("appActivity", snapChatAppActivity);

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);


        waitForVisibilityOf(logInBtn);
        driver.findElement(logInBtn).click();
        driver.findElement(usernameOrEmailField).click();

        String snapChatID = ExcelRead.readData(excelPath, "0", rowNo, "2");
        driver.findElement(usernameOrEmailField).sendKeys(snapChatID);
        System.out.println("SnapChat id copied from excel: "+snapChatID);
        snapChatAc = snapChatID;
        ExcelWrite.writeData(excelPathReport, 0, 2, 2, snapChatID);

        String snapChatPW = ExcelRead.readData(excelPath, "0", rowNo, "3");
        driver.findElement(passwordField).click();
        driver.findElement(passwordField).sendKeys(snapChatPW);
        System.out.println("SnapChat PW copied from excel");

        driver.findElement(signInBtn).click();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);


        if (driver.findElements(errorMessageLogin).size() > 0){
            System.out.println("User Id or password or something wrong: "+snapChatID);
        }else {
            waitForVisibilityOf(friendsBtn);
            System.out.println("Logged in successfully for: "+snapChatID);
            driver.findElement(myProfile).click();
            waitForVisibilityOf(addFriendsBtn);
            driver.findElement(addFriendsBtn).click();
            Thread.sleep(1000);

            //For accepting all friend request
            if (driver.findElements(acceptBtn).size() > 0){
                boolean bool = true;

                while (bool == true){
                    driver.findElement(acceptBtn).click();
                    Thread.sleep(500);
                    if (driver.findElements(acceptBtn).size() <= 0){
                        bool = false;
                        System.out.println("All friend request accepted :)");
                    }
                }
            }else {
                System.out.println("Friend request not available :(");
            }

            Thread.sleep(1000);
            driver.navigate().back();
            waitForVisibilityOf(addFriendsBtn);
            driver.navigate().back();
            waitForVisibilityOf(friendsBtn);

            driver.findElement(friendsBtn).click();
            waitForVisibilityOf(newMessageBtn);
            driver.findElement(newMessageBtn).click();
            Thread.sleep(1000);


            //For finding how many friends in list and collect username

            List <WebElement> list1 = driver.findElements(By.id("name"));

            while (chk == true){
                sizeBeforeScrolling = treeSet.size();

                if (counter == true){
                    swipeVertical(driver, 0.7, 0.2, 0.5, 3000);
                    Thread.sleep(1000);
                }else {
                    swipeVertical(driver, 0.7, 0.5, 0.5, 3000);
                    counter = true;
                }

                for(int i = 0; i < list1.size(); i++){
                    String nameStr = list1.get(i).getText();
                    treeSet.add(nameStr);
                }

                sizeAfterScrolling = treeSet.size();
                if ((sizeBeforeScrolling - sizeAfterScrolling) == 0){
                    chk = false;
                }
            }

            int totalNumberOfFriend = treeSet.size();

            System.out.println("Total number of Friend = "+totalNumberOfFriend);
            Iterator<String> iterator = treeSet.iterator();

            driver.findElement(crossBtn).click();
            waitForVisibilityOf(friendSearchField);
            System.out.println("On the friends page");

            while (iterator.hasNext()){
                String item = iterator.next();
                ExcelWrite.writeData(excelPathReport, 0, friendRowNo, 3, item);

                driver.findElement(friendSearchField).click();
                Thread.sleep(1000);
                driver.findElement(friendSearchField).clear();
                driver.findElement(friendSearchField).sendKeys(item);
                System.out.println("Name = "+item);

                Thread.sleep(3000);
                if (driver.findElements(noResultsTextView).size() > 0){
                    System.out.println("No Result found");
                    ExcelWrite.writeData(excelPathReport, 0, friendRowNo, 4, "NA");
                }else {
                    WebElement recBtn = driver.findElement(MobileBy.id("primary_text"));
                    new TouchAction(driver).press(recBtn).waitAction(Duration.ofMillis(1500)).release().perform();

                    waitForVisibilityOf(settings);
                    driver.findElement(settings).click();
                    Thread.sleep(3000);
                    if (driver.findElements(findUsername).size() <= 0){
                        System.out.println("Username: "+item);
                        ExcelWrite.writeData(excelPathReport, 0, friendRowNo, 4, item);

                    }else {
                        String usernameStr = driver.findElement(findUsername).getText();
                        System.out.println("Username: "+usernameStr);
                        ExcelWrite.writeData(excelPathReport, 0, friendRowNo, 4, usernameStr);
                    }
                    driver.findElement(cancelBtn).click();
                    waitForVisibilityOf(settings);
                    driver.navigate().back();
                    waitForVisibilityOf(friendSearchField);
                }
                friendRowNo ++;
            }

            driver.navigate().back();
            waitForVisibilityOf(friendSearchField);
            driver.navigate().back();
            Thread.sleep(1000);
            driver.navigate().back();

            waitForVisibilityOf(myProfile);
            driver.findElement(myProfile).click();
            waitForVisibilityOf(addFriendsBtn);
            driver.findElement(addFriendsBtn).click();
            waitForVisibilityOf(searchField);

            //For sending random friend request
           if(totalNumberOfFriend < adjustNumberOfFriend){
                for (int i = 1; i <= maxNumberOfFriend; i++){
                    Random r = new Random();
                    int randomValue = r.nextInt(high - low) + low;
                    System.out.println("Random friend number: "+randomValue);

                    String friendsId = ExcelRead.readData(excelPath, "1", randomValue, "2");

                    driver.findElement(searchField).click();
                    driver.findElement(searchField).clear();
                    driver.findElement(searchField).sendKeys(friendsId);
                    Thread.sleep(2000);

                    if (driver.findElements(addBtn).size() > 0){
                        waitForClickabilityOf(addBtn);
                        driver.findElement(addBtn).click();
                        Thread.sleep(2000);
                        if (driver.findElements(snapBtn).size() > 0){
                            System.out.println("Friend request sent to: "+friendsId);
                            ExcelWrite.writeData(excelPathReport, 0, (i + 1), 5, friendsId);
                        }else {
                            System.out.println("Sorry couldn't find "+friendsId);
                        }
                    }else {
                        if (driver.findElements(snapBtn).size() > 0){
                            System.out.println("Friend request already sent to: "+friendsId);
                        }else if (driver.findElements(noSearchResultMsg).size() > 0){
                            System.out.println("User not found!: "+friendsId);
                        }else {
                            System.out.println("Something wrong to find the user: "+friendsId);
                        }
                    }
                }
            }

            //For blocking the user

           driver.navigate().back();
           waitForVisibilityOf(addFriendsBtn);
           Thread.sleep(1000);
           driver.navigate().back();

           waitForVisibilityOf(friendsBtn);
           driver.findElement(friendsBtn).click();
           waitForVisibilityOf(friendSearchField);

           for (int i = 0; i < blockFriendNumber; i++){
               String blockFriend = ExcelRead.readData(excelPathBlockList, "0", i+2, "2");

               driver.findElement(friendSearchField).click();
               driver.findElement(friendSearchField).clear();
               driver.findElement(friendSearchField).sendKeys(blockFriend);
               Thread.sleep(3000);
               Thread.sleep(3000);

               if (driver.findElements(noSearchResultMsg).size() > 0){
                   System.out.println("No Result found");
               }else if (driver.findElements(searchReault).size() > 0){
                   WebElement recBtn = driver.findElement(MobileBy.id("primary_text"));
                   new TouchAction(driver).press(recBtn).waitAction(Duration.ofMillis(1500)).release().perform();

                   waitForVisibilityOf(settings);
                   driver.findElement(settings).click();

                   waitForVisibilityOf(blockBtn);
                   driver.findElement(blockBtn).click();

                   waitForVisibilityOf(yesBtn);
                   driver.findElement(yesBtn).click();
                   System.out.println("Blocked: "+blockFriend);

                   Thread.sleep(1000);
                   driver.navigate().back();
                   waitForVisibilityOf(friendSearchField);
               }else {
                   System.out.println("No Result found");
               }
           }

            //Closing and Clearing the SnapChat app data
            driver.resetApp();
            System.out.println("Closing and Clearing the SnapChat app data");
        }
    }

    @AfterMethod
    public void tearDownAndSendReportToMail() {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        Session session = Session.getDefaultInstance(props,

                new javax.mail.Authenticator() {
                    protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                        return new javax.mail.PasswordAuthentication(emailId, emailPassword);
                    }
                });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(reportSendTo));
            message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(reportSendTo));
            message.setSubject("Test Report for "+snapChatAc);
            BodyPart messageBodyPart1 = new MimeBodyPart();

            messageBodyPart1.setText("Friend list report with username");
            MimeBodyPart messageBodyPart2 = new MimeBodyPart();

            String filename = excelPathReport;
            DataSource source = new FileDataSource(filename);
            messageBodyPart2.setDataHandler(new DataHandler(source));
            messageBodyPart2.setFileName(filename);
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart2);
            multipart.addBodyPart(messageBodyPart1);
            message.setContent(multipart);
            Transport.send(message);

            System.out.println("Email Sent successfully");

        } catch (MessagingException e) {

            throw new RuntimeException(e);

        }

        driver.quit();
    }



    protected void waitForVisibilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    protected void waitForClickabilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }

    //This method for scrolling screen

    public static void swipeVertical(AppiumDriver driver, double startPercentage, double finalPercentage, double anchorPercentage, int duration) throws Exception {
        Dimension size = driver.manage().window().getSize();
        int anchor = (int) (size.width * anchorPercentage);
        int startPoint = (int) (size.height * startPercentage);
        int endPoint = (int) (size.height * finalPercentage);
        new TouchAction(driver).press(anchor, startPoint).waitAction(Duration.ofMillis(duration)).moveTo(anchor, endPoint).release().perform();
    }

}
