package etc;
/*
 *  ****************************************************************************
 *  * Created by : Al Imran on 6/25/2018 at 1:41 PM.
 *  * Email : imranreee@gmail.com
 *  *
 *  * Last edited by : Al Imran on 6/25/2018.
 *  *
 *  * Last Reviewed by : <Reviewer Name> on <mm/dd/yy>
 *  ****************************************************************************
 */

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import settings.HelperClass;

public class UpAndRunVpn extends HelperClass {
    By skipBtn = By.id("textSkip");
    By userNameFieldVpn = By.id("com.protonvpn.android:id/email");
    By passwordFieldVpn = By.id("com.protonvpn.android:id/password");
    By logInBtnVpn = By.id("buttonLogin");

    By gotItBtn = By.id("buttonGotIt");
    By australia = By.xpath("//*[@class='android.widget.LinearLayout' and @width>0 and @height>0 and ./*[@id='imageCountry'] and ./*[@text='Australia']]");
    By usa = By.xpath("//*[@text='United States']");
    By usaUs = By.xpath("//*[@id='radioServer' and (./preceding-sibling::* | ./following-sibling::*)[@text='US']]");


    By australiaAu = By.id("//*[@id='radioServer' and (./preceding-sibling::* | ./following-sibling::*)[@text='AU']]");
    By connectBtn = By.id("buttonConnect");
    By disconnectBtn = By.id("buttonDisconnect");


    public UpAndRunVpn(WebDriver driver) {
        super(driver);
    }

    public UpAndRunVpn upAndRunVpn() throws Exception {
        Thread.sleep(3000);
        if (driver.findElements(userNameFieldVpn).size() > 0){
            driver.findElement(userNameFieldVpn).click();
            driver.findElement(userNameFieldVpn).sendKeys("imranreee");
            driver.findElement(passwordFieldVpn).click();
            driver.findElement(passwordFieldVpn).sendKeys("730729");
            Thread.sleep(1000);
            driver.findElement(logInBtnVpn).click();

            waitForVisibilityOf(gotItBtn);
            driver.findElement(gotItBtn).click();
        }


        driver.findElement(usa).click();
        driver.findElement(usaUs).click();
        Thread.sleep(500);
        driver.findElement(connectBtn).click();
        waitForVisibilityOf(disconnectBtn);
        System.out.println("VPN connected");

        driver.quit();

        //((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.HOME);

        return new UpAndRunVpn(driver);
    }
}
