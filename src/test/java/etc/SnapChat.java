package etc;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import settings.HelperClass;

public class SnapChat extends HelperClass {
    By logInBtn = By.id("login_and_signup_page_fragment_login_button");
    By usernameOrEmailField = By.id("username_or_email_field");
    By passwordField = By.id("password_field");
    By signInBtn = By.id("registration_nav_container");
    By turnOnBtn = By.id("turn_on_button");
    By friendsBtn = By.id("hova_nav_feed_layout");
    By goChat = By.id("feed_item_foreground");
    By mediaGalleryButton = By.id("media_gallery_button");
    By selectImage = By.id("gallery_grid_item_selection_frame");
    By sendBtn = By.id("memories_drawer_send");

    public SnapChat(WebDriver driver) {
        super(driver);
    }

    public SnapChat snapChat() throws Exception {
        //AndroidSetup.prepareAndroidForAppium();

        waitForVisibilityOf(logInBtn);
        driver.findElement(logInBtn).click();
        driver.findElement(usernameOrEmailField).click();
        driver.findElement(usernameOrEmailField).sendKeys("Maxime3231");

        driver.findElement(passwordField).click();
        driver.findElement(passwordField).sendKeys("tester234");

        driver.findElement(signInBtn).click();

        if (driver.findElements(turnOnBtn).size() > 0){
            driver.findElement(turnOnBtn).click();
        }

        waitForVisibilityOf(friendsBtn);
        driver.findElement(friendsBtn).click();
        waitForVisibilityOf(goChat);
        driver.findElement(goChat).click();
        Thread.sleep(1000);
        driver.findElement(mediaGalleryButton).click();

        waitForVisibilityOf(selectImage);
        driver.findElement(selectImage).click();
        driver.findElement(sendBtn).click();
        Thread.sleep(3000);
        ((AndroidDriver)driver).resetApp();

        return new SnapChat(driver);
    }
}
