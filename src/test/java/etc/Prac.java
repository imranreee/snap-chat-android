package etc;

/*
 *  ****************************************************************************
 *  * Created by : Al Imran on 6/25/2018 at 1:41 PM.
 *  * Email : imranreee@gmail.com
 *  *
 *  * Last edited by : Al Imran on 6/25/2018.
 *  *
 *  * Last Reviewed by : <Reviewer Name> on <mm/dd/yy>
 *
 *     This class for reading incoming snap and sending snap
 *  ****************************************************************************
 */

import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;
import settings.ExcelRead;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Prac {

    protected AndroidDriver driver;
    int rowNo = 1;
    String excelPath = "D:\\GitLab\\SnapChat\\logInInfo\\data.xlsx";


    By logInBtn = By.id("login_and_signup_page_fragment_login_button");
    By usernameOrEmailField = By.id("username_or_email_field");
    By passwordField = By.id("password_field");
    By signInBtn = By.id("registration_nav_container");

    By friendsBtn = By.id("hova_nav_feed_layout");
    By goChat = By.id("feed_item_foreground");
    By mediaGalleryButton = By.id("media_gallery_button");
    By selectImage = By.id("gallery_grid_item_selection_frame");
    By sendBtn = By.id("memories_drawer_send");

    By chooseLocation = By.id("com.expressvpn.vpn:id/location_picker");
    By connectedStatus = By.xpath("//*[@text='VPN is ON']");
    By connectedBtn = By.id("bob_power_green");

    @BeforeMethod
    public void setUp() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("deviceName", "Android");
        capabilities.setCapability("platformVersion", "8.1");
        capabilities.setCapability("autoGrantPermissions", "true");

        capabilities.setCapability("unicodeKeyboard", "true");
        capabilities.setCapability("resetKeyboard", "true");
        //capabilities.setCapability("noReset", "true");

        capabilities.setCapability("appPackage", "com.snapchat.android");
        capabilities.setCapability("appActivity", "com.snapchat.android.LandingPageActivity");

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
    }


    @Test(invocationCount = 4)
    public void bscCaseOne() throws Exception{

        waitForVisibilityOf(logInBtn);
        driver.findElement(logInBtn).click();
        driver.findElement(usernameOrEmailField).click();

        rowNo = rowNo + 1;
        String snapChatID = ExcelRead.readData(excelPath, "0", rowNo, "2");
        driver.findElement(usernameOrEmailField).sendKeys(snapChatID);

        String snapChatPW = ExcelRead.readData(excelPath, "0", rowNo, "3");
        driver.findElement(passwordField).click();
        driver.findElement(passwordField).sendKeys(snapChatPW);

        driver.findElement(signInBtn).click();
        waitForVisibilityOf(friendsBtn);
        driver.findElement(friendsBtn).click();

        waitForVisibilityOf(goChat);
        driver.findElement(goChat).click();
        Thread.sleep(1000);
        driver.findElement(mediaGalleryButton).click();

        waitForVisibilityOf(selectImage);
        driver.findElement(selectImage).click();
        driver.findElement(sendBtn).click();
        Thread.sleep(3000);
        Thread.sleep(3000);
        driver.resetApp();

        Thread.sleep(3000);
        String expressVpnAppPackage = "com.expressvpn.vpn";
        String expressVpnAppActivity = "com.expressvpn.vpn.MainActivity";

        //Opening VPN app
        Activity activity = new Activity(expressVpnAppPackage, expressVpnAppActivity);
        driver.startActivity(activity);
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);

        if (driver.findElements(connectedStatus).size() > 0){
            driver.findElement(connectedBtn).click();
            Thread.sleep(3000);
        }

        driver.findElement(chooseLocation).click();
        Thread.sleep(1000);

        //String readLocation = ExcelRead.readData(excelPath, "0", rowNo, "4");
        By selectLocation = By.xpath(ExcelRead.readData(excelPath, "0", rowNo, "4"));

        driver.findElement(selectLocation).click();
        waitForVisibilityOf(connectedStatus);

        driver.pressKeyCode(AndroidKeyCode.HOME);
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

    protected void waitForVisibilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

}
