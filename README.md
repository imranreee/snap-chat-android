# Why this project?
This is automation project for SnapChat android application

# Info
* Project Type: Maven
* Framework: Appium, TestNG

# Prerequisites to use the project
* Download JDK: http://www.oracle.com/technetwork/java/javase/downloads/jdk10-downloads-4416644.html
* Set environment variable for JDK: https://www.mkyong.com/java/how-to-set-java_home-on-windows-10/
* Download SDK: https://developer.android.com/studio/index.html
* Set environment variable for SDK: http://www.software-testing-tutorials-automation.com/2015/09/set-androidhome-and-path-environment.html
* Install and run  Appium server: https://appium.io 
* Download IntelliJ IDEA: https://www.jetbrains.com/idea/download/



# How to run?
* Run Appium Server 
![] (imageForGitLab/6.png)

* Wait for the below screen, if it's appear Appium server is ok
![] (imageForGitLab/7.png)

* Enable developer option from device Settings > Connect Android device with PC via USB cable
* Open command prompt > Type adb devices like 1 > Hit enter > It will show the connected device list like 2
![] (imageForGitLab/8.png)
* Open the given project by IntelliJ IDEA
* Open `FriendRequestSendAccept/ReadAndSendSnap` class and right click on the class name > Run like below image
![] (imageForGitLab/1.png)
* Change the values for how many account do you want to run or how many friend request do you want to send at a time
![] (imageForGitLab/2.png)
![] (imageForGitLab/3.png)
* Export the HTML report like below images
![] (imageForGitLab/4.png)
![] (imageForGitLab/5.png)

# Note: 
* Will send report to your mail for each sanpChat account with how many friends and there username, for that you have to provide below credentials like image
![] (imageForGitLab/9.png)

* Prefer mail: google mail 
* Before running the test case you have to enable less secure app settings for gmail account(for which account you have provided id and password): 
* How to enable: https://stackoverflow.com/questions/25341198/javax-mail-authenticationfailedexception-is-thrown-while-sending-email-in-java
* 